const expect = require('chai').expect;
const mylib = require('../src/mylib');

const a = 4; // Test values 
const b = 3;

describe("Testing mylib", () => {
    it("Can add 1 and 1 together", () => {
        const result = mylib.add(a, b);
        expect(result).to.equal(a+b); // Expects 4+3=7
    });
    it("Can subtract 2 from 1", () => {
        const result = mylib.subtract(a,b);
        expect(result).to.equal(a-b); // Expects 4-3=1
    });
    it("Can multiply 4 and 4", () => {
        const result = mylib.multiply(a,b);
        expect(result).to.equal(a*b); // Expects 4*3=12
    });
    it("Can divide 8 and 2", () => {
        const result = mylib.divide(a,b);
        expect(result).to.equal(a/b); // Expects 4/3=1.3333...
    });
    it("Can handle zero division", () => {
        const result = () => {
            mylib.divide(4, 0)        // Should throw an error "ZeroDivision"
        };
        expect(result).to.throw(Error, "ZeroDivision");
    });
})