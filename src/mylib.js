// Function of addition, Return result a+b
const add = (a, b) => {
    return a + b;
};
// Function for subtraction, Return result of a-b
const subtract = (a, b) => {
    return a - b;
};
// Function for multiplication, Return result of a*b
const multiply = (a, b) => {
    return a * b;
};
// Function for division, Return result of a/b. Throws error if divisor = 0
const divide = (a, b) => {
    if(b == 0) {
        throw new Error("ZeroDivision");
    } else {
        return a / b;
    }
}; 

module.exports = {
    add, subtract, multiply, divide
}