const express = require("express");
const mylib = require("./mylib.js")
const app = express();

const PORT = 3000;
const HOST = "127.0.0.1";

app.get('/', (req,res) => {
    res.send("Default page"); 
});

app.get("/add", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const resultString = mylib.add(a,b).toString();
    res.send(resultString);
});

app.get("/subtract", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const resultString = mylib.subtract(a,b).toString();
    res.send(resultString);
});

app.get("/multiply", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const resultString = mylib.multiply(a,b).toString();
    res.send(resultString);
});

app.get("/divide", (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const resultString = mylib.divide(a,b).toString();
    res.send(resultString);
});

app.listen(PORT, HOST, () => {
    console.log(`Server: http://${HOST}:${PORT}`)
});