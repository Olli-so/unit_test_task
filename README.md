# Unit testing task return
This repository includes package.json which has all of the needed dependencies, mylib.js which does calculations, main.js where a node server is setup and run and mylib.test.js which is made to unit test the functionality of mylib.js functions.

In this task the goal was to familiarize our selfs with chai, mocha and make a few simple unit tests for calculative functions to check their functionality.